from .models import Account
from .serializers import AccountSerializer
from .variables import REG_OAUTH_URL, TOKEN_OAUTH_URL
from .helpers import encrypt_password, check_password, update_user

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

import requests

import logging
from logstash_async.handler import AsynchronousLogstashHandler
logger = logging.getLogger("logstash")      
logger.setLevel(logging.INFO)

# Create the handler
handler = AsynchronousLogstashHandler(
    host='343e0cf4-eb64-4642-8aed-f8a94ed80b47-ls.logit.io', 
    port=10112,
    ssl_enable=False,
    ssl_verify=False,
    database_path=''
)

# Assign handler to the logger
logger.addHandler(handler)

class AccountView(APIView):

    def get(self, request, format=None):
        account = Account.objects.all().order_by('username')
        serializer = AccountSerializer(account, many=True)
        logger.info("[GET] user/ 200 OK")
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = AccountSerializer(data=request.data)
        if serializer.is_valid():
            hashed_password = encrypt_password(serializer.validated_data['password'])
            data = {
                "username": serializer.validated_data['username'],
                "password": hashed_password,
                "full_name": serializer.validated_data['full_name'],
                "client_id": "47",
                "client_secret": "accountlawa7",
                "role": serializer.validated_data['role']
            }
            url = REG_OAUTH_URL
            call_oauth = requests.post(url, data=data)
            resp = call_oauth.json()
            if 'error' in resp:
                if (resp['error_description'] == 'user already existed'):
                    url = TOKEN_OAUTH_URL
                    data = {
                        "username": serializer.validated_data['username'],
                        "password": hashed_password,
                        "grant_type": "password",
                        "client_id": "47",
                        "client_secret": "accountlawa7"
                    }
                    call_oauth = requests.post(url, data=data)
                    resp = call_oauth.json()
            
            if 'error' not in resp:
                serializer.validated_data['password'] = hashed_password
                serializer.save()
                resp_data = {
                    "username": serializer.data['username'],
                    "password": hashed_password,
                    "full_name": serializer.data['full_name'],
                    "role": serializer.data['role'],
                    "phone_number": serializer.data['phone_number'],
                    "profile_picture": serializer.data['profile_picture'],
                    "access_token": resp['access_token'],
                    "expires_in": resp['expires_in'],
                    "refresh_token": resp['refresh_token'],
                    "scope": resp['scope'],
                    "token_type": resp['token_type'],
                }

                logger.info("[POST] user/ 200 OK")
                return Response(resp_data, status=status.HTTP_201_CREATED)
            else:
                logger.info("[POST] user/ 400 Bad Request")
                return Response(resp, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AccountDetailView(APIView):
    def get(self, request, username, format=None):
        try:
            account = Account.objects.get(username=username)
            serializer = AccountSerializer(account)
            logger.info("[POST] /user 200 OK")
            return Response(serializer.data)
        except Account.DoesNotExist:
            logger.info("[POST] /user 404 Error")
            return Response(
                {'response': 'account with username: ' + str(username) + ' not found'},
                status=404
            )

    def patch(self, request, username, format=None):
        try: 
            response = request.data
            account = Account.objects.get(username=username)
            params = [
                'username', 'password', 'full_name', 'role', 
                'phone_number', 'profile_picture'
            ]
            if 'username' in response:
                try:
                    account = Account.objects.get(username=response['username'])
                    if username != response['username']:
                        serializer = AccountSerializer(account)
                        logger.info("[PATCH] /user 404 Error")
                        return Response(
                            {'response': 'account with username: ' + response['username'] + ' already exists.'},
                            status=404
                        )
                    else: 
                        account = update_user(username, account, params, response)
                        serializer = AccountSerializer(account)
                        logger.info("[PATCH] /user 200 OK")
                        return Response(serializer.data, status=200)
                except Account.DoesNotExist:
                    account = update_user(username, account, params, response)
                    serializer = AccountSerializer(account)
                    logger.info("[PATCH] /user 200 OK")
                    return Response(serializer.data, status=200)
            else:
                account = update_user(username, account, params, response)
                serializer = AccountSerializer(account)
                logger.info("[PATCH] /user 200 OK")
                return Response(serializer.data, status=200)
        except Account.DoesNotExist:
            logger.info("[PATCH] /user 404 Error")
            return Response(
                {'response': 'account with username: ' + str(username) + ' not found'},
                status=404
            )

    def delete(self, request, username, format=None):
        try:
            account = Account.objects.get(username=username)
            account.delete()
            logger.info("[DELETE] /user 200 OK")
            return Response(
                {'response': 'account with username: ' + str(username) + ' deleted'},
                status=200
            )
        except Account.DoesNotExist:
            logger.info("[DELETE] /user 404 Error")
            return Response(
                {'response': 'account with username: ' + str(username) + ' not found'},
                status=404
            )

class LoginView(APIView):
    def post(self, request, format=None):
        try:
            response = request.data
            account = Account.objects.get(username=response['username'])
            serializer = AccountSerializer(account)
            if not check_password(response['password'], serializer.data['password']):
                logger.info("[POST] /login 404 Error")
                return Response(
                    {'response': 'username or password invalid'},
                    status=404
                )

            url = TOKEN_OAUTH_URL
            data = {
                "username": serializer.data['username'],
                "password": serializer.data['password'],
                "grant_type": "password",
                "client_id": "47",
                "client_secret": "accountlawa7"
            }
            call_oauth = requests.post(url, data=data)
            resp = call_oauth.json()
            
            if 'error' not in resp:
                resp_data = {
                    "access_token": resp['access_token'],
                    "refresh_token": resp['refresh_token'],
                    "role": serializer.data['role'],
                }
                logger.info("[POST] /login 200 OK")
                return Response(resp_data, status=200)
            else:
                return Response(resp, status=400)
        except Account.DoesNotExist:
            logger.info("[POST] /login 200 OK")
            return Response(
                {'response': 'account with username: ' + str(request.data['username']) + ' not found'},
                status=404
            )
