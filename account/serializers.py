from rest_framework import serializers
from .models import Account

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = [
            'id', 'username', 'password', 'full_name', 'role',
            'phone_number', 'profile_picture'
        ]
