from .variables import UPD_OAUTH_URL
import bcrypt
import requests


def encrypt_password(raw_pass):
    enc_pass = bcrypt.hashpw(raw_pass.encode('utf-8'), bcrypt.gensalt())
    enc_pass = enc_pass.decode('utf-8')
    return enc_pass

def check_password(in_raw_pass, db_enc_pass):
    in_raw_pass = in_raw_pass.encode('utf-8')
    db_enc_pass = db_enc_pass.encode('utf-8')

    if bcrypt.checkpw(in_raw_pass, db_enc_pass):
        return True
    return False

def update_user(old_username, account, params, response):
    for param in params:
        if param in response:
            if response[param] != None or response[param] != "":
                if param == "password":
                    enc_pass = encrypt_password(response[param])
                    setattr(account, param, enc_pass)
                else:
                    setattr(account, param, response[param])

    account.save()
    data = {
        "username": account.username,
        "password": account.password,
        "full_name": account.full_name,
    }
    url = UPD_OAUTH_URL
    call_oauth = requests.patch(url+old_username, data=data)
    return account