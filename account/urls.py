from django.urls import path
from .views import AccountView, AccountDetailView, LoginView

urlpatterns = [
    path('user/', AccountView.as_view(), name='url_account'),
    path('user/<str:username>', AccountDetailView.as_view(), name='url_account_detail'),
    path('login/', LoginView.as_view(), name='url_login'),
]
