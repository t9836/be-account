from django.db import models

# Create your models here.
class Account(models.Model):
    username = models.CharField(max_length=255, unique=True)
    password = models.CharField(max_length=255)
    full_name = models.CharField(max_length=255)
    role = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=255)
    profile_picture = models.ImageField()

    def __str__(self):
        return self.username